const { Datastore } = require('@google-cloud/datastore');
const express = require('express');
const cors = require('cors');
const axios = require('axios');

const app = express();
app.use(express.json());
app.use(cors());
const datastore = new Datastore();

app.patch('/users/login', async (req, res) => {
    const info = req.body;
    const key = datastore.key(['user', info.email]);
    const response = await datastore.get(key);
    const user = response[0]
    if (user instanceof Object) {
        if (info.password === user.password) {
            res.status(200);
            res.send({ fname: user.fname, lname: user.lname });
        } else {
            res.status(404);
            res.send('Incorrect Password');
        }
    } else {
        res.status(404);
        res.send('Email Does Not Exist');
    }
})

app.get('/users/:email/verify', async (req, res) => {
    const key = datastore.key(['user', req.params.email]);
    const response = await datastore.get(key);
    const user = response[0];
    if (user instanceof Object) {
        res.status(200);
        res.send(user.email);
    } else {
        res.status(404);
        res.send('User Does Not Exist.');
    }
})

app.get('/messages', async (req, res) => {
    if (req.query.recipient && req.query.sender) {
        const query = datastore.createQuery('message').filter('sender', '=', req.query.sender).filter('recipient', '=', req.query.recipient);
        const [data, metaInfo] = await datastore.runQuery(query);
        res.send(data);
    } else if (req.query.recipient && !req.query.sender) {
        const query = datastore.createQuery('message').filter('recipient', '=', req.query.recipient);
        const [data, metaInfo] = await datastore.runQuery(query);
        res.send(data);
    } else if (req.query.sender && !req.query.recipient) {
        const query = datastore.createQuery('message').filter('sender', '=', req.query.sender);
        const [data, metaInfo] = await datastore.runQuery(query);
        res.send(data);
    } else {
        const query = datastore.createQuery('message');
        const [data, metaInfo] = await datastore.runQuery(query);
        res.send(data);
    }
})

app.get('/messages/:mid', async (req, res) => {
    const key = datastore.key(['message', Number(req.params.mid)]);
    const message = await datastore.get(key);
    if (message[0] == null) {
        res.status(404);
        res.send(`No Message Found with ID ${req.params.mid}`);
    } else {
        res.send(message)
    }
})

app.post('/messages', async (req, res) => {
    try {
        const message = req.body;
        await axios.get(`https://wedding-planner-service.uk.r.appspot.com/users/${message.sender}/verify`);
        await axios.get(`https://wedding-planner-service.uk.r.appspot.com/users/${message.recipient}/verify`);
        message.timestamp = new Date().toLocaleString();
        const key = datastore.key(['message']);
        const response = await datastore.save({ key: key, data: message });
        res.status(201);
        res.send('Message added Successfully.')
    } catch (error) {
        res.status(400);
        res.send('User email does not exist.');
    }
})

const PORT = process.env.PORT
app.listen(PORT, () => console.log("App Started"));