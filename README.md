# Wedding Planner

## Project Description
The Wedding Planner app is an online planner for a small wedding planner company. Employees to create and manage weddings. Employees can also send each other messages. It is a micro-services cloud deployed application. The application makes use of many GCP services including, Google Compute Engine, Datastore, Cloud SQL, Datastore, App Engine, Cloud Functions, Cloud Storage and Firebase Hosting.

## Technologies Used
NodeJS
Express.js
React
GCP Compute Engine
GCP Cloud Storage
GCP Cloud SQL
GCP Cloud Functions
GCP App Engine
GCP Datastore

## Features

React frontend with working authorization service so unauthorized users can't access anywhere except the login page
Wedding Planner Service is a Full-Stack REST API which includes Entities, Persistence Layer, Service Layer, and Interface Layer
Full implementation of Authorization and Messaging services with GCP Datastore
Frontend pushes expense images up to GCP Cloud Storage for persistence

## To-do list

Need to fix issue with updating an expense where you can update the attachment after creation
Add pictures to make the design look better
Come up with some branding for aesthetic purposes

# Getting Started

### Frontend
```bash
git clone https://gitlab.com/baeder-wedding-planner/wedding-frontend.git
```
### Wedding Planner REST API
```bash
git clone https://gitlab.com/baeder-wedding-planner/weddingplanner.git
```
### Authorization and Messaging Service
```bash
git clone https://gitlab.com/baeder-wedding-planner/wedding-datastore.git
```

All endpoints were set up for my GCP project, so IP addresses and URLs need to fit your own projects on GCP
Database credentials should be changed to fit your own database on GCP
The REST API was hosted on GCP Compute Engine, but could be run locally for ease of access
The Authorization and Messaging Service was hosted on GCP App Engine
The Frontend was hosted on Firebase, but could be run locally for ease of access
Need to set up GOOGLE_APPLICATION_CREDENTIALS for authorization in GCP
